
// Script en JavaScript pour faite aparaitre le bouton "Retour haut de page" à partir de 650px
window.onscroll = function () {
    scrollFunction()
};

function scrollFunction() {
    if (document.body.scrollTop > 500 || document.documentElement.scrollTop > 500) {
        document.getElementById("GoToTopBtn").style.display = "block";
    } else {
        document.getElementById("GoToTopBtn").style.display = "none";
    }
}

// Scroll vers le haut quand l'utilisateur clique sur le bouton
function topFunction() {
    document.body.scrollTop = 0;
    document.documentElement.scrollTop = 0;
}


// Boutpn de langues 
const frBtn = document.querySelector('.fr-btn');
const enBtn = document.querySelector('.en-btn');

const frTitle = document.querySelector('.fr-title');
const enTitle = document.querySelector('.en-title');

const frIntro = document.querySelector('.fr-intro');
const enIntro = document.querySelector('.en-intro');

const frSkills = document.querySelector('.fr-skills');
const enSkills = document.querySelector('.en-skills');

const frWork = document.querySelector('.fr-work');
const enWork = document.querySelector('.en-work');

const frSchool = document.querySelector('.fr-school');
const enSchool = document.querySelector('.en-school');

const frContact = document.querySelector('.fr-contact');
const enContact = document.querySelector('.en-contact');

let defaultFrLang = false;

function toggleLanguage() {
    if(!defaultFrLang) {
        // Display FR Button
        frBtn.style.display = "block";
        enBtn.style.display = "none";
        // Display Text in English
        frTitle.style.display = 'none';
        enTitle.style.display = 'block';

        frIntro.style.display = 'none';
        enIntro.style.display = 'block';

        frSkills.style.display = 'none';
        enSkills.style.display = 'block';

        frWork.style.display = 'none';
        enWork.style.display = 'block';

        frSchool.style.display = 'none';
        enSchool.style.display = 'block';

        frContact.style.display = 'none';
        enContact.style.display = 'block';

        document.getElementById("BtnText").innerHTML = "Back to Top";

        defaultFrLang = true;
    }
    else {
        // Display EN Button
        frBtn.style.display = "none";
        enBtn.style.display = "block";
        // Display Text in French
        frTitle.style.display = 'block';
        enTitle.style.display = 'none';

        frIntro.style.display = 'block';
        enIntro.style.display = 'none';

        frSkills.style.display = 'block';
        enSkills.style.display = 'none';

        frWork.style.display = 'block';
        enWork.style.display = 'none';

        frSchool.style.display = 'block';
        enSchool.style.display = 'none';

        frContact.style.display = 'block';
        enContact.style.display = 'none';

        document.getElementById("BtnText").innerHTML = "Retour haut de Page";

        defaultFrLang = false;
    }
}